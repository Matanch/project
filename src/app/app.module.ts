import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';

import { AppComponent } from './app.component';

// Angular Material 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatCardModule} from '@angular/material/card';
import { MatExpansionModule} from '@angular/material/expansion';
import { MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule,ReactiveFormsModule}   from '@angular/forms';
import { NavComponent } from './components/nav/nav.component';
import { BuildcvComponent } from './components/buildcv/buildcv.component';
import { ClassifycvComponent } from './components/classifycv/classifycv.component';
import { JobslistComponent } from './components/jobslist/jobslist.component';
import { FavjobsComponent } from './components/favjobs/favjobs.component';
import { RouterModule, Routes } from '@angular/router';
import { ClassifiedComponent } from './components/classified/classified.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFirestore} from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthService } from './auth.service';
import {MatInputModule} from '@angular/material/input';
import { WelcomeComponent } from './welcome/welcome.component';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MycvComponent } from './components/mycv/mycv.component';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatTooltipModule} from '@angular/material/tooltip';


const appRoutes: Routes = [
   { path: 'buildcv', component: BuildcvComponent },
   { path: 'classifycv', component: ClassifycvComponent },
   { path: 'favjobs', component: FavjobsComponent },
   { path: 'jobslist', component: JobslistComponent },
   { path: 'classified', component: ClassifiedComponent },
   { path: 'welcome', component: WelcomeComponent },

  { path: "",
    redirectTo: '/welcome',
    pathMatch: 'full'
  },
];
@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BuildcvComponent,
    ClassifycvComponent,
    JobslistComponent,
    FavjobsComponent,
    ClassifiedComponent,
    WelcomeComponent,
    MycvComponent
  ],
  imports: [
    BrowserModule ,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    BrowserModule,
    BrowserAnimationsModule,
    MatSliderModule,
    LayoutModule,
    MatInputModule,
    MatToolbarModule,
    MatButtonModule,
    MatDatepickerModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatCardModule,
    MatTooltipModule,
    MatExpansionModule,
    MatNativeDateModule,
    MatFormFieldModule,
    AngularFireAuthModule,
    FormsModule,
    CdkTreeModule,
    ReactiveFormsModule,
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
  ],
  providers: [AngularFireAuth,AngularFirestore,AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
