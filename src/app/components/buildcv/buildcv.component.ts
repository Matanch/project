import { ICv } from './../../helpers/ICv';
import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import {TooltipPosition} from '@angular/material/tooltip';


@Component({
  selector: 'app-buildcv',
  templateUrl: './buildcv.component.html',
  styleUrls: ['./buildcv.component.css']
})
export class BuildcvComponent implements OnInit {

  positionOptions: TooltipPosition[] = ['above'];
  position = new FormControl(this.positionOptions[0]);
  
  public cvObject: ICv;
  isDisplay = true;
  hiddenBtn = false;
  panelOpenState = false;
  educationDisplay = true;
  constructor() { }

  ngOnInit() {
    this.cvObject = {
      phoneNumber: '',
      email: '',
      firstName: '',
      lastName: '',
    };
  }

  toggleDisplay(){
    this.isDisplay = !this.isDisplay;
    this.hiddenBtn = !this.hiddenBtn;
  }

    educationtoggle(){
      this.educationDisplay  =!this.educationDisplay;
    }
}
