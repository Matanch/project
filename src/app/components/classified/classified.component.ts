import { ClassifycvService } from './../../classifycv.service';
import { Component, OnInit } from '@angular/core';
//import { ImageService } from '../image.service';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string;
  
  constructor(public classifyservice:ClassifycvService ) { }

              ngOnInit() {
                this.classifyservice.classify().subscribe(
                  res => {
                    this.category = this.classifyservice.categories[res];
                    //this.categoryImage = this.imageservice.images[res];
                  }
                )
              }
            

}
