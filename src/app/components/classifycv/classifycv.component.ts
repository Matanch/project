import { ClassifycvService } from './../../classifycv.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-classifycv',
  templateUrl: './classifycv.component.html',
  styleUrls: ['./classifycv.component.css']
})
export class ClassifycvComponent implements OnInit {

  constructor(private classifyservice:ClassifycvService,private router:Router) { }

text:string;


  ngOnInit() {
  }

  onSubmit(){
    this.classifyservice.doc = this.text;
    this.router.navigate(['/classified']);
  }


}