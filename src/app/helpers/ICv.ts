export interface ICv {
    phoneNumber: string;
    email: string;
    firstName: string;
    lastName: string;
}