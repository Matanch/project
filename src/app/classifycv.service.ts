import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClassifycvService {

  private url = "https://dl2qgy18x8.execute-api.us-east-1.amazonaws.com/beta";

  public categories:object = {0: 'Project management', 1: 'Photography', 2: 'Data analyst', 3: 'Team Leader', 4: 'CTO'};
  public doc:string;

  constructor(private http:HttpClient) { }

  classify():Observable<any>{
    let json = {
      "articles": [
        {"text": this.doc}
      ]
    }
    let body = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        let final = res.body.replace('[', '');
        final = final.replace(']', '');
        return final;
      })
    )
  }
}
